'use strict';

/**
 * @ngdoc function
 * @name m5sampleApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the m5sampleApp
 */
angular.module('m5sampleApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
