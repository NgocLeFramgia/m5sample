'use strict';

/**
 * @ngdoc function
 * @name m5sampleApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the m5sampleApp
 */
angular.module('m5sampleApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
